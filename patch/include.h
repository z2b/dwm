/* Bar functionality */
#include "bar_indicators.h"
#include "bar_tagicons.h"
#include "bar.h"

#include "bar_ltsymbol.h"
#include "bar_status.h"
#include "bar_status2d.h"
#include "bar_tags.h"

/* Other patches */
#include "attachx.h"
#include "cyclelayouts.h"
#include "restartsig.h"
#include "scratchpad.h"
#include "seamless_restart.h"
#include "swallow.h"
#include "vanitygaps.h"
#include "xrdb.h"
/* Layouts */
#include "layout_bstack.h"
#include "layout_centeredmaster.h"
#include "layout_monocle.h"
#include "layout_tile.h"


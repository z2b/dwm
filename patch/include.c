/* Bar functionality */
#include "bar_indicators.c"
#include "bar_tagicons.c"
#include "bar.c"

#include "bar_ltsymbol.c"
#include "bar_status.c"
#include "bar_status2d.c"
#include "bar_tags.c"

/* Other patches */
#include "attachx.c"
#include "cyclelayouts.c"
#include "restartsig.c"
#include "scratchpad.c"
#include "swallow.c"
#include "vanitygaps.c"
#include "xrdb.c"
#include "seamless_restart.c"
/* Layouts */
#include "layout_facts.c"
#include "layout_bstack.c"
#include "layout_centeredmaster.c"
#include "layout_monocle.c"
#include "layout_tile.c"

